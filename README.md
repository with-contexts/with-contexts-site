WITH-CONTEXTS
=============

Marco Antoniotti
See file COPYING for licensing information


DESCRIPTION
-----------

This library contains an implementation of a `WITH` macro with
"contexts" inspired by Python, which, in turn was obviously inspired
by Common Lisp macros (and other earlier languages, like Pascal).

The present project contains the documentation for the `WITH-CONTEXS`
library. Plese refer to [project
page](https://gitlab.common-lisp.net/mantoniotti/with-contexts) to see
the actual code and some explanation.

Here you can find the actual
[documentation](https://common-lisp.net/project/with-contexts).


A NOTE ON FORKING
-----------------

Of course you are free to fork the project subject to the current
licensing scheme.  However, before you do so, I ask you to consider
plain old "cooperation" by asking me to become a developer.
It helps keeping the entropy level at an acceptable level.

---

Enjoy!

Marco Antoniotti
2023-01-21

